//
//  ArtiMediaSDK.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 09/10/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AMSDK.
FOUNDATION_EXPORT double AMSDKVersionNumber;

//! Project version string for AMSDK.
FOUNDATION_EXPORT const unsigned char AMSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AMSDK/PublicHeader.h>

#import <AMSDK/AMSDK.h>
#import <AMSDK/AMSDKAPI.h>
#import <AMSDK/AMInitParams.h>
#import <AMSDK/AMEventType.h>
#import <AMSDK/AMEventDelegate.h>
#import <AMSDK/AMInitParamsConfig.h>
#import <AMSDK/AMInitJsonBuilder.h>


