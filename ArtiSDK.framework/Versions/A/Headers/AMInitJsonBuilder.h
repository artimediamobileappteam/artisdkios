//
//  AMInitJsonBuilder.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 10/06/2019.
//  Copyright © 2019 Michael Avoyan. All rights reserved.
//

#import "AMInitParamsConfig.h"

extern NSString * const PARAMS_VERSION;

@interface AMInitJsonBuilder : NSObject

@property (nonatomic, strong) NSMutableDictionary* params;

- (instancetype)initWithParams:(NSMutableDictionary*)params;

- (AMInitJsonBuilder*)putPlacementSiteKey:(NSString*)value;
- (NSString*)getPlacementSiteKey;
- (AMInitJsonBuilder*)putPlacementCategory:(NSString*)value;
- (AMInitJsonBuilder*)putPlacementIsLive:(BOOL)value;
- (AMInitJsonBuilder*)putContentId:(NSString*)value;
- (NSString*)getContentId;
- (AMInitJsonBuilder*)putContentDuration:(NSString*)value;
- (AMInitJsonBuilder*)putContentType:(NSString*)value;
- (AMInitJsonBuilder*)putContentProgramName:(NSString*)value;
- (AMInitJsonBuilder*)putContentSeason:(NSString*)value;
- (AMInitJsonBuilder*)putContentEpisode:(NSString*)value;
- (AMInitJsonBuilder*)putContentGenre:(NSString*)value;
- (AMInitJsonBuilder*)putContentProvider1:(NSString*)value;
- (AMInitJsonBuilder*)putContentProvider2:(NSString*)value;
- (AMInitJsonBuilder*)putContentProvider3:(NSString*)value;
- (AMInitJsonBuilder*)putContentTargetAudience:(NSString*)value;
- (AMInitJsonBuilder*)putContentCuePoints:(NSArray*)array;
- (AMInitJsonBuilder*)putContentVideoUrl:(NSString*)value;
- (NSString*)getContentVideoUrl;
- (AMInitJsonBuilder*)putContentPublisherId:(NSString*)value;
- (NSString*)getContentPublisherId;
- (AMInitJsonBuilder*)putSubscriberId:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberPlan:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberCountry:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberState:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberCity:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberZipCode:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberType:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberBirthYear:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberGender:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberLanguage:(NSString*)value;
- (AMInitJsonBuilder*)putSubscriberReligion:(NSString*)value;
- (AMInitJsonBuilder*)putTechnicalPlatform:(NSString*)value;
- (AMInitJsonBuilder*)putTechnicalIsExternalAdPlayer:(BOOL)value;
- (BOOL)getTechnicalIsExternalAdPlayer;
- (AMInitJsonBuilder*)putTechnicalDefaultInitTimeout;
- (AMInitJsonBuilder*)putTechnicalInitTimeout:(int)value;
- (int)getTechnicalInitTimeout;
- (AMInitJsonBuilder*)putTechnicalAid;
- (NSString*)getTechnicalAid;
- (AMInitJsonBuilder*)putTechnicalIsLAT;
- (NSString*)getTechnicalIsLAT;
- (AMInitJsonBuilder*)putTechnicalUuid:(NSString*)value;
- (AMInitJsonBuilder*)putTechnicalLocationLat:(double)value;
- (AMInitJsonBuilder*)putTechnicalLocationLon:(double)value;
- (NSMutableDictionary*)toDictionary;

@end
