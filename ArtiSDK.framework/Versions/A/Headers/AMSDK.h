//
//  AMSDK.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 14/09/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMSDKAPI.h"

@interface AMSDK : NSObject

+ (id<AMSDKAPI>)getVideoAdvAPI;

@end

