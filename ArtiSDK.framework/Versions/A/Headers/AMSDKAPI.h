//
//  AMSDKAPI.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 14/09/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMInitParams.h"
#import "AMContentState.h"
#import "AMEventType.h"
#import "AMAdEventDelegate.h"

@protocol AMSDKAPI <NSObject>

- (void)initialize:(nonnull AMInitParams*)params NS_SWIFT_NAME(initialize(params:));
- (void)startAdBreak:(float)breakTime breakIndex:(int)breakIndex NS_SWIFT_NAME(startAdBreak(breakTime:breakIndex:));
- (void)stopAdBreak NS_SWIFT_NAME(stopAdBreak());
- (void)updateDisplayFrame:(CGRect)frame NS_SWIFT_NAME(updateDisplayFrame(frame:));
- (void)updateVideoState:(AMContentState)state NS_SWIFT_NAME(updateVideoState(state:));
- (void)updateVideoTime:(float)time NS_SWIFT_NAME(updateVideoTime(time:));
- (void)registerEvent:(AMEventType)event eventDelegate:(nonnull id<AMEventDelegate>)delegate NS_SWIFT_NAME(registerEvent(event:eventDelegate:));
- (void)setAdVolume:(float)volumeLevel NS_SWIFT_NAME(setAdVolume(volumeLevel:));
- (void)pauseAd NS_SWIFT_NAME(pauseAd());
- (void)resumeAd NS_SWIFT_NAME(resumeAd());
- (float)getAdCurrentTime NS_SWIFT_NAME(getAdCurrentTime());
- (float)getAdDuration NS_SWIFT_NAME(getAdDuration());
- (void)destroy NS_SWIFT_NAME(destroy());

/**
 * API to play Ad by external player:
 */
- (NSString*_Nullable)getVideoAdUrl;
- (void)updateVideoAdTime:(float)time andDuration:(float)duration NS_SWIFT_NAME(updateVideoAdTime(time:duration:));
- (void)showAdOverlay NS_SWIFT_NAME(showAdOverlay());
- (void)hideAdOverlay NS_SWIFT_NAME(hideAdOverlay());
- (void)errorAddPlay:(nonnull NSString*)errorDesc NS_SWIFT_NAME(errorAddPlay(errorDesc:));

@end

