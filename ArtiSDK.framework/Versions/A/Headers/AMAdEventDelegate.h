//
//  AMEventDelegate.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 14/09/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMEventType.h"

@protocol AMEventDelegate <NSObject>

- (void)onAMSDKEvent:(AMEventType)event eventData:(nullable NSObject*)data;

@end

