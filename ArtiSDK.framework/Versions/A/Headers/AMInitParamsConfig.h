//
//  AMInitParamsConfig.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 10/06/2019.
//  Copyright © 2019 Michael Avoyan. All rights reserved.
//

extern NSString* const PLATFORM_TYPE_HDK;
extern int const DEFAULT_INITIALIZATION_TIMEOUT_IN_SECONDS;

extern NSString* const KEY_PLACEMENT;
    extern NSString* const KEY_PLACEMENT_SITE_KEY;
    extern NSString* const KEY_PLACEMENT_CATEGORY;
    extern NSString* const KEY_PLACEMENT_IS_LIVE;

extern NSString* const KEY_CONTENT;
    extern NSString* const KEY_CONTENT_ID;
    extern NSString* const KEY_CONTENT_DURATION;
    extern NSString* const KEY_CONTENT_TYPE;
    extern NSString* const KEY_CONTENT_PROGRAM_NAME;
    extern NSString* const KEY_CONTENT_SEASON;
    extern NSString* const KEY_CONTENT_EPISODE;
    extern NSString* const KEY_CONTENT_GENRE;
    extern NSString* const KEY_CONTENT_PROVIDER1;
    extern NSString* const KEY_CONTENT_PROVIDER2;
    extern NSString* const KEY_CONTENT_PROVIDER3;
    extern NSString* const KEY_CONTENT_TARGET_AUDIENCE;
    extern NSString* const KEY_CONTENT_CUE_POINTS;
    extern NSString* const KEY_CONTENT_VIDEO_URL;
    extern NSString* const KEY_CONTENT_PUBLISHER_ID;

extern NSString* const KEY_SUBSCRIBER;
    extern NSString* const KEY_SUBSCRIBER_ID;
    extern NSString* const KEY_SUBSCRIBER_PLAN;
    extern NSString* const KEY_SUBSCRIBER_COUNTRY;
    extern NSString* const KEY_SUBSCRIBER_STATE;
    extern NSString* const KEY_SUBSCRIBER_CITY;
    extern NSString* const KEY_SUBSCRIBER_ZIPCODE;
    extern NSString* const KEY_SUBSCRIBER_TYPE;
    extern NSString* const KEY_SUBSCRIBER_BIRTH_YEAR;
    extern NSString* const KEY_SUBSCRIBER_GENDER;
    extern NSString* const KEY_SUBSCRIBER_LANGUAGE;
    extern NSString* const KEY_SUBSCRIBER_RELIGION;

extern NSString* const KEY_TECHNICAL;
    extern NSString* const KEY_TECHNICAL_PARAMS_VERSION;
    extern NSString* const KEY_TECHNICAL_PLATFORM;
    extern NSString* const KEY_TECHNICAL_INIT_TIMEOUT;
    extern NSString* const KEY_TECHNICAL_IS_EXTERNAL_AD_PLAYER;
    extern NSString* const KEY_TECHNICAL_ID;
        extern NSString* const KEY_TECHNICAL_ID_AID;
        extern NSString* const KEY_TECHNICAL_ID_IS_LAT;
        extern NSString* const KEY_TECHNICAL_ID_UUID;
    extern NSString* const KEY_TECHNICAL_LOCATION;
        extern NSString* const KEY_TECHNICAL_LOCATION_LAT;
        extern NSString* const KEY_TECHNICAL_LOCATION_LON;
    extern NSString* const KEY_TECHNICAL_APP;
        extern NSString* const KEY_TECHNICAL_APP_HDK_VERSION;
        extern NSString* const KEY_TECHNICAL_APP_VERSION;
        extern NSString* const KEY_TECHNICAL_APP_BUNDLE;
    extern NSString* const KEY_TECHNICAL_DEVICE;
        extern NSString* const KEY_TECHNICAL_DEVICE_OS;
        extern NSString* const KEY_TECHNICAL_DEVICE_MODEL;
        extern NSString* const KEY_TECHNICAL_DEVICE_NETWORK;
