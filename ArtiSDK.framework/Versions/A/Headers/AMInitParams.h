//
//  AMInitParams.h
//  ArtiSDK
//
//  Created by Michael Avoyan on 14/09/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMInitJsonBuilder.h"

@interface AMInitParams : NSObject

@property (nonatomic, strong) UIView* targetUIView;
@property (nonatomic, strong) AMInitJsonBuilder* jsonBuilder;

- (instancetype)initWithTargetUIView:(UIView*)targetUIView params:(NSDictionary*)params;
- (instancetype)initWithTargetUIView:(UIView*)targetUIView initJsonBuilder:(AMInitJsonBuilder*)initJsonBuilder;
- (NSString*)getVideoUrl NS_SWIFT_NAME(getVideoUrl());
- (NSString*)getPublisherID NS_SWIFT_NAME(getPublisherID());
- (NSString*)getVideoID NS_SWIFT_NAME(getVideoID());
- (int)getInitTimeout:(int)defaultInitializationTimeoutInSeconds NS_SWIFT_NAME(getInitTimeout(defaultInitializationTimeoutInSeconds:));
- (BOOL)isExternalAdPlayer NS_SWIFT_NAME(isExternalAdPlayer());
- (void)setLat:(double)lat NS_SWIFT_NAME(setLat(lat:));
- (void)setLon:(double)lon NS_SWIFT_NAME(setLon(lon:));
- (void)setUuid:(NSString*)uuid NS_SWIFT_NAME(setUuid(uuid:));
- (NSMutableDictionary*)toDictionary NS_SWIFT_NAME(toDictionary());

@end

