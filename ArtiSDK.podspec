Pod::Spec.new do |s|
s.name             = 'ArtiSDK'
s.module_name      = 'ArtiSDK'
s.version          = '1.4.000'
s.summary          = 'Artimedia framework'
s.author           = { 'Michael Avoyan' => 'michael.avoyan@arti-media.net' }

s.description   = <<-DESC
Artimedia is an advanced ad-exchange platform that manages and delivers a wide range of Ad types. Along
with standard Linear/None-Linear advertisements, Artimedia delivers content-synced ads, leveraging
advanced machine-vision analysis of video content.
DESC
s.homepage         = 'http://arti-media.net/en/'

s.license          = { :type => 'COMMERCIAL', :text => 'http://arti-media.net/en/terms-of-use/' }
# s.source           = { :git => 'https://Michael_Avoyan@bitbucket.org/artimediamobileappteam/artisdkios', :tag => s.version.to_s }
s.source           = { :git => 'https://bitbucket.org/artimediamobileappteam/artisdkios.git', :tag => s.version.to_s }

s.platform         = :ios, '7.0'
s.requires_arc     = true

## This is the compiled framework with all the sources inside
s.vendored_frameworks = 'ArtiSDK.framework'
s.preserve_paths = 'ArtiSDK.framework/*'

## Source files (if there are source files)
#    s.source_files = 'Documentation/*'
#    s.resources = 'Pod/Assets/*'
#    s.source_files = 'Pod/Classes/**/*'
#    s.ios.source_files = '*.{h,m,mm,framework}'
#    s.resources       = 'Documentation/*'
s.documentation_url = 'http://docs.advsnx.net/ArtiSdk/iOS/ArtiMediaSDK-iOS-ObjC-Integration.pdf'

s.frameworks      = 'WebKit'
#    s.files = Dir["lib/**/*.rb"] + %w{ bin/pod bin/sandbox-pod README.md LICENSE CHANGELOG.md }

## Project configurations:
s.xcconfig =  { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}

## Resource bundles (if there are assets that should be imported to the hosting project as bundle)
#    s.resource_bundles = {'ArtiSDK' => ['ArtiSDK/*.{framework}']}
#    s.resource_bundles = {'ArtiSDK' => ['*.{framework}']}

#     s.resource_bundles = {
#      'ArtiSDK' => ['Assets/*.{png,bundle,xib,lproj}']
#    }

## Dependencies (if there are dependencies)
#   s.dependency 'MySomeDependancy', '~> 1.2.3'

end
