
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <ArtiSDK/AMSDK.h>
#import "AMVideo.h"

#define SITE_KEY                    @"bayontv"
#define CATEGORY                    @"test"
#define VIDEO_ID                    @"mobileAppTest"
#define USER_TYPE                   @"0"// 1 - is subscribed
#define INIT_TIMEOUT_IN_SECONDS     7.0

#define IS_MY_PLAYER_PLAYS_AD       NO

//    Content details values:
#define CD_DURATION_IN_SECONDS      600.0f
#define CD_EPISODE_NAME             @"episode name"
#define CD_GENRE_NAME               @"genre name"
#define CD_PROGRAM_NAME             @"program name"
#define CD_SEASON_NAME              @"season name"

#define UPDATE_AD_TIME_DELAY        0.5

@interface ViewController : AMVideo <AMEventDelegate>
{
@private
    
    BOOL isSdkInitialized;
    BOOL adSessionAlive;
    BOOL isLiveAdBreakOn;
    BOOL adPlayInProgress;
    BOOL isToUpdateAMSDKThread;
    int breakIndex;
}
@property (nonatomic, strong) id <AMSDKAPI> amsdkapi;
@property (nonatomic, strong) UIView* adContainerView;
@property (nonatomic) BOOL wantToAdjustOverlayToDisplayedVideo;

@end

