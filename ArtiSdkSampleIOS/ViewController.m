
#import "ViewController.h"

@implementation ViewController

- (void)viewDidLayoutSubviews
{
    if(_amsdkapi)
        [_amsdkapi updateDisplayFrame:CGRectMake(0, 0, _adContainerView.frame.size.width, _adContainerView.frame.size.height)];
    
    _wantToAdjustOverlayToDisplayedVideo = IS_MY_PLAYER_PLAYS_AD && adPlayInProgress;
}
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator
{
    // best call super just in case
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // will execute before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context)
     {
         // will execute during rotation
         
     } completion:^(id  _Nonnull context)
     {
         // will execute after rotation
         
         if(self.amsdkapi){
             [self.amsdkapi updateDisplayFrame:CGRectMake(0, 0, self.adContainerView.frame.size.width, self.adContainerView.frame.size.height)];
         }
     }];
}

- (void)allocUI
{
    [super allocUI];
    
    [self.btnAdBreakLive addTarget:self action:@selector(onStartLiveAdBreak) forControlEvents:UIControlEventTouchUpInside];
    
    _adContainerView = [[UIView alloc]init];
    [super.bgSV addSubview:_adContainerView];
}

- (void)initUiFrames
{
    [super initUiFrames];
    
    _adContainerView.frame = super.contentVideoView.frame;
}
- (void)updateLinearAdBreakTitle:(BOOL)isOn
{
    isLiveAdBreakOn = isOn;
    [super updateLinearAdBreakTitle:isOn];
}
- (void)START {
    [super START];
    
    [self initSDK];
}
- (void)playerPlaybackDidFinish:(NSNotification*)notification
{
    if(IS_MY_PLAYER_PLAYS_AD && adPlayInProgress) {
        NSLog(@"Video Ad Completed...");
        [self updateAdVideoTimeFromMyPlayer:YES];
    }
    else {
        NSLog(@"Video Content Completed...");
        if( _amsdkapi && !adPlayInProgress ) {
            [_amsdkapi updateVideoState:VIDEO_STATE_STOP];
        }
        [super playerPlaybackDidFinish:notification];
    }
}
- (void)playerFailedToPlay:(NSNotification*)notification
{
    if(IS_MY_PLAYER_PLAYS_AD && adPlayInProgress) {
        NSLog(@"Failed to play video ad:\n%@", notification.description);
        if( _amsdkapi ) {
            [_amsdkapi errorAddPlay:notification.description];
        }
        adPlayInProgress = NO;
    }
    else {
        NSLog(@"Failed to play video content:\n%@", notification.description);
        if( _amsdkapi && !adPlayInProgress ) {
            [_amsdkapi updateVideoState:VIDEO_STATE_STOP];
        }
    }
}
- (void)initSDK
{
    breakIndex = 0;
    
    _amsdkapi = [AMSDK getVideoAdvAPI];
    if( _amsdkapi )
    {
        [_amsdkapi registerEvent:EVT_INIT_COMPLETE eventDelegate:self];
        [_amsdkapi registerEvent:EVT_PAUSE_REQUEST eventDelegate:self];
        [_amsdkapi registerEvent:EVT_RESUME_REQUEST eventDelegate:self];
        [_amsdkapi registerEvent:EVT_LINEAR_AD_START eventDelegate:self];
        [_amsdkapi registerEvent:EVT_LINEAR_AD_PAUSE eventDelegate:self];
        [_amsdkapi registerEvent:EVT_LINEAR_AD_RESUME eventDelegate:self];
        [_amsdkapi registerEvent:EVT_LINEAR_AD_STOP eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_MISSED eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_SHOW eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_HIDE eventDelegate:self];
        [_amsdkapi registerEvent:EVT_SESSION_END eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_CLICK eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_SCREEN_TOUCH_DOWN eventDelegate:self];
        [_amsdkapi registerEvent:EVT_AD_SCREEN_TOUCH_UP eventDelegate:self];
        
        AMInitParams* amInitParams = [[AMInitParams alloc]initWithTargetUIView:_adContainerView initJsonBuilder:[self getInitJsonBuilderParams]];
        [_amsdkapi initialize:amInitParams];
    }
    else
    {
        NSLog(@"ArtiSDK Allocation failed. => Continue without ArtiSDK.");
    }
}

- (void)resetAMSdk
{
    breakIndex = 0;
    
    adPlayInProgress = NO;
    [self stopUpdateAMSDKUIThread];
    
    [self pauseContent];
    
    if( !_amsdkapi )
    {
        [self initSDK];
    }
    else
    {
        AMInitParams* amInitParams = [[AMInitParams alloc]initWithTargetUIView:_adContainerView initJsonBuilder:[self getInitJsonBuilderParams]];
        [_amsdkapi initialize:amInitParams];
    }
}

- (void)destroyAMSdk
{
    adPlayInProgress = NO;
    [self stopUpdateAMSDKUIThread];
    
    [super disableLive];
    
    if( _amsdkapi )
        [_amsdkapi destroy];
    _amsdkapi = nil;
}

- (void)onAMSDKEvent:(AMEventType)event eventData:(nullable NSObject*)data
{
    NSLog(@"PUBLISHER onAMSDKEvent: %@", EventTypeToString(event));
    
    switch (event)
    {
        case EVT_INIT_COMPLETE:
            [self onInitComplete:data];
            break;
        case EVT_PAUSE_REQUEST:
            [self onPauseRequest];
            break;
        case EVT_RESUME_REQUEST:
            [self onResumeRequest];
            break;
        case EVT_LINEAR_AD_START:
            [self onLinearAdStart];
            break;
        case EVT_LINEAR_AD_PAUSE:
            [self onLinearAdPause];
            break;
        case EVT_LINEAR_AD_RESUME:
            [self onLinearAdResume];
            break;
        case EVT_LINEAR_AD_STOP:
            [self onLinearAdStop];
            break;
        case EVT_AD_SHOW:
            NSLog(@"Ad Type: %@", [NSString stringWithFormat:@"%@", data]);
            [self onAdShow];
            break;
        case EVT_AD_HIDE:
            NSLog(@"Ad Type: %@", [NSString stringWithFormat:@"%@", data]);
            [self onAdHide];
            break;
        case EVT_AD_MISSED:
            [self onAdMissed];
            break;
        case EVT_SESSION_END:
            [self onSessionEnd];
            break;
        case EVT_AD_CLICK:
            [self onAdClick];
            break;
        case EVT_AD_SCREEN_TOUCH_DOWN:
//            Do something if needed...
            break;
        case EVT_AD_SCREEN_TOUCH_UP:
//            Do something if needed...
            break;
        case EVT_ERROR:
//            Do nothing...
            break;
    }
}
- (void)onInitComplete:(NSObject*)data
{
    adSessionAlive = [((NSNumber*) data) boolValue];
    
    if ( adSessionAlive ) {
        [super enableLive];
        [super updateLiveButtons];
        if( _amsdkapi ) {
            [_amsdkapi updateVideoState:VIDEO_STATE_PLAY];
        }
    }
    else{
        [self destroyAMSdk];
    }   
    isSdkInitialized = YES;
}

- (void)onPauseRequest
{
    [self pauseContent];
    [super enableResume];
}
- (void)onResumeRequest
{
    [self resumeContent];
    [self enablePause];
}
- (void)onLinearAdStart
{
    adPlayInProgress = YES;
    [self startUpdateAMSDKUIThread];
    [self enablePause];
    if(super.isLive)
        [self updateLinearAdBreakTitle:YES];
}
- (void)onLinearAdPause
{
    [self enableResume];
}
- (void)onLinearAdResume
{
    [self enablePause];
}
- (void)onLinearAdStop
{
    adPlayInProgress = NO;
    [self stopUpdateAMSDKUIThread];
    if(super.isLive)
        [self updateLinearAdBreakTitle:NO];
}
- (void)onAdShow
{
    [super enablePause];
    if(IS_MY_PLAYER_PLAYS_AD)
        [self startAdInMyPlayer];
}
- (void)onAdHide
{
    if(IS_MY_PLAYER_PLAYS_AD)
        [self stopAdInMyPlayer];
}
- (void)onAdMissed
{
    [self resumeContent];
}
- (void)onSessionEnd
{
    adPlayInProgress = NO;
    [self resumeContent];
    [self destroyAMSdk];
}
- (void)onAdClick
{
}

- (void)onPausePressed
{
    if( isSdkInitialized ) {
        if(adPlayInProgress && !IS_MY_PLAYER_PLAYS_AD && _amsdkapi) {
            [_amsdkapi pauseAd];
            [super enableResume];
        }
        else{
            [super onPausePressed];
        }
    }
}
- (void)onResumePressed
{
    if( isSdkInitialized ) {
        if(adPlayInProgress && !IS_MY_PLAYER_PLAYS_AD && _amsdkapi) {
            [_amsdkapi resumeAd];
            [super enablePause];
        }
        else{
            [super onResumePressed];
        }
    }
}

- (void)pauseContent
{
    [super pauseContent];
    
    if (isSdkInitialized) {
        if(_amsdkapi && !adPlayInProgress)
            [_amsdkapi updateVideoState:VIDEO_STATE_PAUSE];
    }
}
- (void)resumeContent
{
    [super resumeContent];
    
    if (isSdkInitialized) {
        if(_amsdkapi && !adPlayInProgress)
            [_amsdkapi updateVideoState:VIDEO_STATE_RESUME];
    }
}
- (void)onNewMovie:(NSString*)url
{
    [super onNewMovie:url];
    [self resetAMSdk];
}
- (void)onIsLive
{
    [super onIsLive];
    [self resetAMSdk];
}
- (void)onStartLiveAdBreak
{
    [self onStartLiveAdBreak:!isLiveAdBreakOn];
}
- (void)onStartLiveAdBreak:(BOOL)isOn
{
    isLiveAdBreakOn = isOn;
    if( _amsdkapi ){
        if( isLiveAdBreakOn )
            [_amsdkapi startAdBreak:[super getLiveBreakDuration] breakIndex:++breakIndex];
        else
            [_amsdkapi stopAdBreak];
    }
}
- (float)getCurrentAdDuration
{
    float duration = 0;
    if( adPlayInProgress && _amsdkapi ) {
        if( IS_MY_PLAYER_PLAYS_AD )
            duration = [super getCurrentContentDuration];
        else
            duration = [_amsdkapi getAdDuration];
    }
    return duration;
}
- (float)getCurrentAdPosition
{
    float position = 0;
    if( adPlayInProgress && _amsdkapi ) {
        if( IS_MY_PLAYER_PLAYS_AD )
            position = [super getCurrentContentPosition];
        else
            position = [_amsdkapi getAdCurrentTime];
    }
    return position;
}
- (void)updateUI
{
    if( adPlayInProgress ) {
        if( IS_MY_PLAYER_PLAYS_AD )
            [self updateAdVideoTimeFromMyPlayer:NO];
        else if( _amsdkapi )
            [super updateSeekBar:[_amsdkapi getAdCurrentTime] deuration:[_amsdkapi getAdDuration]];
    }
    else {
        [super updateUI];
        float position = [super getCurrentContentPosition];
        if( _amsdkapi && position > 0 )
            [_amsdkapi updateVideoTime:position];
    }
}
- (void)startUpdateAMSDKUIThread
{
    if(!isToUpdateAMSDKThread) {
        isToUpdateAMSDKThread = YES;
        dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(backgroundQueue, ^{
            
            [self updateAMSDKUI];
        });
    }
}

- (void)stopUpdateAMSDKUIThread
{
    isToUpdateAMSDKThread = NO;
}
- (void)updateAMSDKUI
{
    while ( isToUpdateAMSDKThread && adPlayInProgress )
    {
        if (adSessionAlive)
        {
            float position = [self getCurrentAdPosition];
            float duration = [self getCurrentAdDuration];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [super updateSeekBar:position deuration:duration];
            });
        }
        sleep(UPDATE_AD_TIME_DELAY);
    }
}

- (AMInitJsonBuilder*)getInitJsonBuilderParams
{
    AMInitJsonBuilder *initJsonBuilder = [[AMInitJsonBuilder alloc] init];
    
    [initJsonBuilder putPlacementSiteKey:SITE_KEY];
    [initJsonBuilder putPlacementCategory:CATEGORY];
    [initJsonBuilder putPlacementIsLive:[self isLive]];
    [initJsonBuilder putContentId:@"content id"];
    [initJsonBuilder putContentDuration:@"content duration"];
    [initJsonBuilder putContentType:@"content type"];
    [initJsonBuilder putContentProgramName:@"content program name"];
    [initJsonBuilder putContentSeason:@"content season"];
    [initJsonBuilder putContentEpisode:@"content episode"];
    [initJsonBuilder putContentGenre:@"content genre"];
    [initJsonBuilder putContentProvider1:@"content provider 1"];
    [initJsonBuilder putContentProvider2:@"content provider 2"];
    [initJsonBuilder putContentProvider3:@"content provider 3"];
    [initJsonBuilder putContentTargetAudience:@"content target audience"];
    [initJsonBuilder putContentCuePoints:[NSMutableArray arrayWithObjects:[NSNumber numberWithDouble:60.368],[NSNumber numberWithDouble:360.004], nil]];
    [initJsonBuilder putContentVideoUrl:[super.currentContentURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    [initJsonBuilder putContentPublisherId:@"content publisher id"];
    [initJsonBuilder putSubscriberId:@"subscriber id"];
    [initJsonBuilder putSubscriberPlan:@"subscriber plan"];
    [initJsonBuilder putSubscriberCountry:@"subscriber country"];
    [initJsonBuilder putSubscriberState:@"subscriber state"];
    [initJsonBuilder putSubscriberCity:@"subscriber city"];
    [initJsonBuilder putSubscriberType:@"subscriber type"];
    [initJsonBuilder putSubscriberBirthYear:@"subscriber birth year"];
    [initJsonBuilder putSubscriberGender:@"subscriber gender"];
    [initJsonBuilder putSubscriberLanguage:@"subscriber language"];
    [initJsonBuilder putSubscriberReligion:@"subscriber religion"];
    
    return initJsonBuilder;
}

/**
 * Video Ad By My Player Implementation:
*/
- (void)startAdInMyPlayer
{
    if( _amsdkapi )
    {
        adPlayInProgress = YES;
        
        [super.playerViewController.player pause];
        NSString* adUrl = [_amsdkapi getVideoAdUrl];
        [super setUrlToPlayer:adUrl updateDelay:UPDATE_AD_TIME_DELAY];
        [super.playerViewController.player seekToTime:kCMTimeZero];
        [super.playerViewController.player play];
        _wantToAdjustOverlayToDisplayedVideo = YES;
    }
}
- (void)stopAdInMyPlayer
{
    if( _amsdkapi )
    {
        adPlayInProgress = NO;
        
        [_amsdkapi hideAdOverlay];
        [super.playerViewController.player pause];
        [super  setContentUrl:super.currentContentURL];
        [super seekToCurrentUrlPosition];
    }
}
- (void)updateAdVideoTimeFromMyPlayer:(BOOL)isFinishedAdPlay
{
    if (adPlayInProgress)
    {
        float duration = [self getCurrentAdDuration];
        float position = duration;
        
        if( !isFinishedAdPlay )
            position = [self getCurrentAdPosition];
        
        if (position > 0 && duration > 0)
        {
            [_amsdkapi updateVideoAdTime:position andDuration:duration];
            
            [super updateSeekBar:position deuration:duration];
            
            if( _wantToAdjustOverlayToDisplayedVideo ) {
                [_amsdkapi updateDisplayFrame:super.playerViewController.videoBounds];
                _wantToAdjustOverlayToDisplayedVideo = NO;
                [_amsdkapi showAdOverlay];
            }
        }
    }
}

@end
