//
//  main.m
//  AMSdkSampleIOS
//
//  Created by Michael Avoyan on 26/10/2017.
//  Copyright © 2017 Michael Avoyan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
