//
//  AMVideo.m
//  ArtiSdkSampleIOS
//
//  Created by Michael Avoyan on 07/01/2019.
//  Copyright © 2019 Michael Avoyan. All rights reserved.
//

#import "AMVideo.h"

@implementation AMVideo

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ( [self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    [self initContentPositionMap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPause)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onResume)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    _isLive = NO;
    
    [self allocUI];
    [self initUiFrames];
    
    [self disablePauseResume];
    
    [self disableLive];
}

// Like in Android
- (void)onPause
{
    [self onPausePressed];
}
// Like in Android
- (void)onResume
{
    [self onResumePressed];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self initUiFrames];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator
{
    // best call super just in case
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // will execute before rotation
    
    [coordinator animateAlongsideTransition:^(id  _Nonnull context)
     {
         // will execute during rotation
         
     } completion:^(id  _Nonnull context)
     {
         // will execute after rotation
         
         [self initUiFrames];
     }];
}

- (void)initUiFrames
{
    CGRect windowRect = [[UIScreen mainScreen] bounds];
    
    NSLog(@"Window Rect (%d, %d, %d, %d)", (int)windowRect.origin.x, (int)windowRect.origin.y, (int)windowRect.size.width, (int)windowRect.size.height);
    
    _bgSV.frame = windowRect;
    
    _btnIsLive.frame = CGRectMake(CONTROLLERS_MARGIN_SIDE*2, LIVE_BREAK_Y, LIVE_BUTTONS_W, LIVE_BUTTONS_H);
    
    _btnAdBreakLive.frame = CGRectMake(_btnIsLive.frame.origin.x + _btnIsLive.frame.size.width + CONTROLLERS_MARGIN_SIDE*2,
                                       _btnIsLive.frame.origin.y, LIVE_AD_BREAK_BUTTONS_W, LIVE_BUTTONS_H);
    
    _tfLiveAdBreakTime.frame = CGRectMake(_btnAdBreakLive.frame.origin.x + _btnAdBreakLive.frame.size.width + CONTROLLERS_MARGIN_SIDE,
                                          _btnAdBreakLive.frame.origin.y, LIVE_TEXT_FIELD_W, LIVE_BUTTONS_H);
    
    int w = windowRect.size.width;
    int h = windowRect.size.height*VIDEO_H_PERC/100;
    CGRect videoRect = CGRectMake(0, _btnIsLive.frame.origin.y+_btnIsLive.frame.size.height+CONTROLLERS_MARGIN_TOP, w, h);
    
    NSLog(@"Video Rect (%d, %d, %d, %d)", (int)videoRect.origin.x, (int)videoRect.origin.y, (int)videoRect.size.width, (int)videoRect.size.height);
    
    _contentVideoView.frame = videoRect;
    
    _playerViewController.view.frame = CGRectMake(0, 0, _contentVideoView.frame.size.width, _contentVideoView.frame.size.height);
    
    float pX = (windowRect.size.width - PLAYER_CONTROLLERS_SIZE)/2;
    float pY = _contentVideoView.frame.origin.y + _contentVideoView.frame.size.height + CONTROLLERS_MARGIN_TOP;
    _playBtn.frame = CGRectMake(pX, pY, PLAYER_CONTROLLERS_SIZE, PLAYER_CONTROLLERS_SIZE);
    
    _pauseBtn.frame = _playBtn.frame;
    
    float prW = windowRect.size.width*PROGRESSBAR_W_PERC/100;
    float prX = (windowRect.size.width - prW)/2;
    float prY = pY + _playBtn.frame.size.height + CONTROLLERS_MARGIN_TOP;
    _progressBar.frame = CGRectMake(prX, prY, prW, PROGRESSBAR_H);
    
    float x = (windowRect.size.width - MOVIES_BUTTONS_W)/2;
    float y = _progressBar.frame.origin.y + _progressBar.frame.size.height + CONTROLLERS_MARGIN_TOP;
    _btnNewPlayerMovie1.frame = CGRectMake(x, y, MOVIES_BUTTONS_W, MOVIES_BUTTONS_H);
    
    y = _btnNewPlayerMovie1.frame.origin.y + _btnNewPlayerMovie1.frame.size.height;
    _btnNewPlayerMovie2.frame = CGRectMake(x, y, MOVIES_BUTTONS_W, MOVIES_BUTTONS_H);
    
    y = _btnNewPlayerMovie2.frame.origin.y + _btnNewPlayerMovie2.frame.size.height;
    _btnSamePlayerMovie1.frame = CGRectMake(x, y, MOVIES_BUTTONS_W, MOVIES_BUTTONS_H);
    
    y = _btnSamePlayerMovie1.frame.origin.y + _btnSamePlayerMovie1.frame.size.height;
    _btnSamePlayerMovie2.frame = CGRectMake(x, y, MOVIES_BUTTONS_W, MOVIES_BUTTONS_H);
    
    float sX = (windowRect.size.width - START_SIZE)/2;
    float sY = windowRect.size.height - CONTROLLERS_MARGIN_TOP - START_SIZE;
    _startBtn.frame = CGRectMake(sX, sY, START_SIZE, START_SIZE);
    
    CGSize bgSVSize = CGSizeMake(windowRect.size.width, _btnSamePlayerMovie2.frame.origin.y + _btnSamePlayerMovie2.frame.size.height + CONTROLLERS_MARGIN_TOP);
    
    [_bgSV setContentSize:bgSVSize];
}
- (void)allocUI
{
    _bgSV = [[UIScrollView alloc]init];
    _bgSV.hidden = YES;
    [self.view addSubview:_bgSV];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    _btnIsLive = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnIsLive setTitle:TITLE_NOT_LIVE forState:UIControlStateNormal];
    [_btnIsLive setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnIsLive setBackgroundImage:[UIImage imageNamed:@"red_button.png"] forState:UIControlStateNormal];
    [_btnIsLive addTarget:self action:@selector(onIsLive) forControlEvents:UIControlEventTouchUpInside];
    [_bgSV addSubview:_btnIsLive];
    
    _btnAdBreakLive = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnAdBreakLive setTitle:TITLE_START_LIVE_AD_BREAK forState:UIControlStateNormal];
    [_btnAdBreakLive setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnAdBreakLive setBackgroundImage:[UIImage imageNamed:@"red_button.png"] forState:UIControlStateNormal];
    _btnAdBreakLive.hidden = YES;
    [_bgSV addSubview:_btnAdBreakLive];
    
    _tfLiveAdBreakTime = [[UITextField alloc] init];
    [_tfLiveAdBreakTime setText:LIVE_BREAK_DEFAULT_TIME];
    [_tfLiveAdBreakTime setBackground:[UIImage imageNamed:@"tf_bg.png"]];
    [_tfLiveAdBreakTime addTarget:self action:@selector(onLiveAdBreakTimeInputDidChange) forControlEvents:UIControlEventEditingChanged];
    _tfLiveAdBreakTime.keyboardType = UIKeyboardTypeNumberPad;
    _tfLiveAdBreakTime.textColor = [UIColor whiteColor];
    _tfLiveAdBreakTime.textAlignment = NSTextAlignmentCenter;
    _tfLiveAdBreakTime.hidden = YES;
    [_bgSV addSubview:_tfLiveAdBreakTime];
    
    _contentVideoView = [[UIView alloc]init];
    [_bgSV addSubview:_contentVideoView];
    
    _playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_playBtn setBackgroundImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    [_playBtn addTarget:self action:@selector(onResumePressed) forControlEvents:UIControlEventTouchUpInside];
    [_bgSV addSubview:_playBtn];
    
    _pauseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_pauseBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [_pauseBtn addTarget:self action:@selector(onPausePressed) forControlEvents:UIControlEventTouchUpInside];
    [_bgSV addSubview:_pauseBtn];
    
    _progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressBar.progress = 0;
    [_bgSV addSubview:_progressBar];
    
    _btnNewPlayerMovie1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnNewPlayerMovie1 addTarget:self action:@selector(onNewPlayerMovie1) forControlEvents:UIControlEventTouchUpInside];
    [_btnNewPlayerMovie1 setTitle:@"New Player Movie 1" forState:UIControlStateNormal];
    [_bgSV addSubview:_btnNewPlayerMovie1];
    
    _btnNewPlayerMovie2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnNewPlayerMovie2 addTarget:self action:@selector(onNewPlayerMovie2) forControlEvents:UIControlEventTouchUpInside];
    [_btnNewPlayerMovie2 setTitle:@"New Player Movie 2" forState:UIControlStateNormal];
    [_bgSV addSubview:_btnNewPlayerMovie2];
    
    _btnSamePlayerMovie1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnSamePlayerMovie1 addTarget:self action:@selector(onSamePlayerMovie1) forControlEvents:UIControlEventTouchUpInside];
    [_btnSamePlayerMovie1 setTitle:@"Same Player Movie 1" forState:UIControlStateNormal];
    [_bgSV addSubview:_btnSamePlayerMovie1];
    
    _btnSamePlayerMovie2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnSamePlayerMovie2 addTarget:self action:@selector(onSamePlayerMovie2) forControlEvents:UIControlEventTouchUpInside];
    [_btnSamePlayerMovie2 setTitle:@"Same Player Movie 2" forState:UIControlStateNormal];
    [_bgSV addSubview:_btnSamePlayerMovie2];
    
    _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_startBtn setTitle:TITLE_START forState:UIControlStateNormal];
    [_startBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_startBtn addTarget:self action:@selector(START) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_startBtn];
    
    [self initContentPlayer];
}

- (void)allocPlayerController
{
    NSLog(@"Allocate New Player Controller");
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.view.backgroundColor = [UIColor blackColor];
    _playerViewController.showsPlaybackControls = NO;
    [_contentVideoView addSubview:_playerViewController.view];
    //    [self addPlayerItemObservers];
}
- (void)addPlayerItemObservers
{
    @try{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerPlaybackDidFinish:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:_playerViewController.player.currentItem];
    }
    @catch(NSException *exception){
        NSLog(@"Exception while adding observer to Ad Player: %@", [exception description]);
    }
    @try{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFailedToPlay:)
                                                     name:AVPlayerItemFailedToPlayToEndTimeNotification
                                                   object:_playerViewController.player.currentItem];
    }
    @catch(NSException *exception){
        NSLog(@"Exception while adding observer to Ad Player: %@", [exception description]);
    }
    @try{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFailedToPlay:)
                                                     name:AVPlayerItemNewErrorLogEntryNotification
                                                   object:_playerViewController.player.currentItem];
    }
    @catch(NSException *exception){
        NSLog(@"Exception while removeing observer from Ad Player: %@", [exception description]);
    }
}
- (void)playerPlaybackDidFinish:(NSNotification*)notification
{
}
- (void)playerFailedToPlay:(NSNotification*)notification
{
}
- (void)initContentPositionMap
{
    _contentPositionMap = [[NSMutableDictionary alloc] init];
    [_contentPositionMap setObject:[NSNumber numberWithFloat:0] forKey:URL_NEW_PLAYER_MOVIE_1];
    [_contentPositionMap setObject:[NSNumber numberWithFloat:0] forKey:URL_NEW_PLAYER_MOVIE_2];
    [_contentPositionMap setObject:[NSNumber numberWithFloat:0] forKey:URL_SAME_PLAYER_MOVIE_1];
    [_contentPositionMap setObject:[NSNumber numberWithFloat:0] forKey:URL_NEW_PLAYER_MOVIE_2];
}
- (void)setFrameToPlayerController
{
    _playerViewController.view.frame = CGRectMake(0, 0, _contentVideoView.frame.size.width, _contentVideoView.frame.size.height);
}

- (void)onLiveAdBreakTimeInputDidChange
{
    if( [[_tfLiveAdBreakTime text] length] ){
        _btnAdBreakLive.enabled = YES;
        [_btnAdBreakLive setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else{
        _btnAdBreakLive.enabled = NO;
        [_btnAdBreakLive setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}

- (float)getLiveBreakDuration
{
    return [[_tfLiveAdBreakTime text] floatValue];
}

- (void)updateLinearAdBreakTitle:(BOOL)isOn
{
    if( isOn )
        [_btnAdBreakLive setTitle:TITLE_STOP_LIVE_AD_BREAK forState:UIControlStateNormal];
    else
        [_btnAdBreakLive setTitle:TITLE_START_LIVE_AD_BREAK forState:UIControlStateNormal];
}

- (void)onIsLive
{
    [self disableIsLiveBtn];
    
    _isLive = !_isLive;
    
    [self onPausePressed];
}
- (void)disableIsLiveBtn
{
    _btnIsLive.enabled = NO;
    [_btnIsLive setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}
- (void)enableIsLiveBtn
{
    _btnIsLive.enabled = YES;
    [_btnIsLive setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
- (void)disableLive
{
    [self disableIsLiveBtn];
    [_btnIsLive setTitle:TITLE_NOT_LIVE forState:UIControlStateNormal];
    
    _btnAdBreakLive.hidden = YES;
    _tfLiveAdBreakTime.hidden = YES;
}

- (void)updateLiveButtons
{
    if(_isLive){
        [_btnIsLive setTitle:TITLE_LIVE forState:UIControlStateNormal];
        _btnAdBreakLive.hidden = NO;
        _tfLiveAdBreakTime.hidden = NO;
    }
    else{
        [_btnIsLive setTitle:TITLE_NOT_LIVE forState:UIControlStateNormal];
        _btnAdBreakLive.hidden = YES;
        _tfLiveAdBreakTime.hidden = YES;
    }
}

- (void)enableLive
{
    [self enableIsLiveBtn];
}
- (void)reallocPlayer
{
    [self pauseContent];
    [self allocPlayerController];
    [self setFrameToPlayerController];
}
- (void)initContentPlayer {
    [self allocPlayerController];
    [self setContentUrl:URL_NEW_PLAYER_MOVIE_1];
    [self pauseContent];
}
- (void)onNewMovie:(NSString*)url
{
    [self initContentPositionMap];
    [self setContentUrl:url];
}
- (void)onNewPlayerMovie1
{
    [self reallocPlayer];
    [self onNewMovie:URL_NEW_PLAYER_MOVIE_1];
}

- (void)onNewPlayerMovie2
{
    [self reallocPlayer];
    [self onNewMovie:URL_NEW_PLAYER_MOVIE_2];
}

- (void)onSamePlayerMovie1
{
    [self onNewMovie:URL_SAME_PLAYER_MOVIE_1];
}

- (void)onSamePlayerMovie2
{
    [self onNewMovie:URL_SAME_PLAYER_MOVIE_2];
}

- (void)onResumePressed
{
    [self enablePause];
    [self resumeContent];
}

- (void)onPausePressed
{
    [self enableResume];
    [self pauseContent];
}
- (void)startMediaPlayerContent
{
    [_progressBar setProgress:0 animated:NO];
    [self resumeContent];
    if ([self isPlayEnabled])
        [self pauseContent];
}
- (void)seekToCurrentUrlPosition
{
    [_playerViewController.player seekToTime:CMTimeMakeWithSeconds(((NSNumber*)[_contentPositionMap objectForKey:_currentContentURL]).floatValue, 1)];
}
- (void)setContentUrl:(NSString*)url
{
    _currentContentURL = url;
    
    [self pauseContent];
    [self setUrlToPlayer:_currentContentURL updateDelay:UPDATE_CONTENT_TIME_DELAY];
    [_progressBar setProgress:0 animated:NO];
}
- (void)setUrlToPlayer:(NSString*)url updateDelay:(float)updateDelay
{
    _playerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:url]];
    //    _playerViewController.player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithURL:[NSURL URLWithString:url]]];
    //    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:url]];
    //    if(_playerViewController.player)
    //        [_playerViewController.player replaceCurrentItemWithPlayerItem:playerItem];
    //    else
    //        _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    
    [self addPlayerItemObservers];
    
    __weak typeof(self) weakSelf = self;
    [_playerViewController.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(updateDelay, NSEC_PER_SEC)
                                                               queue:nil
                                                          usingBlock:^(CMTime time)
     {
         [weakSelf updateUI];
     }];
}

- (void)START
{
    _startBtn.hidden = YES;
    _bgSV.hidden = NO;
}

- (void)updateUI
{
    float duration = [self getCurrentContentDuration];
    float position = [self getCurrentContentPosition];
    [self updateSeekBar:position deuration:duration];
    [self updateCurrentPositionMap:position];
}
- (void)updateCurrentPositionMap:(float)position {
    if( position > ((NSNumber*)[_contentPositionMap objectForKey:_currentContentURL]).floatValue ) {
        [_contentPositionMap setObject:[NSNumber numberWithFloat:position] forKey:_currentContentURL];
    }
}
- (void)updateSeekBar:(float)position deuration:(float)duration
{
    if(duration > 0 && position > 0) {
        float progressVal = position / duration;
        if(!isnan(progressVal))
            [_progressBar setProgress:progressVal animated:NO];
    }
}

- (float)getCurrentContentPosition
{
    float playheadPosition = 0;
    if ( _playerViewController.player.currentItem.status == AVPlayerItemStatusReadyToPlay ) {
        playheadPosition = CMTimeGetSeconds(_playerViewController.player.currentItem.currentTime);
    }
    return playheadPosition;
}
- (float)getCurrentContentDuration
{
    float playheadPosition = 0;
    if ( _playerViewController.player.currentItem.status == AVPlayerItemStatusReadyToPlay ) {
        playheadPosition = CMTimeGetSeconds(_playerViewController.player.currentItem.duration);
    }
    return playheadPosition;
}

- (void)pauseContent
{
    if( _playerViewController.player.rate == PLAYER_RATE_PLAYS )
        [_playerViewController.player pause];
}
- (void)resumeContent
{
    if( _playerViewController.player.rate != PLAYER_RATE_PLAYS )
        [_playerViewController.player play];
}

- (void)enablePause {
    _playBtn.hidden = YES;
    _pauseBtn.hidden = NO;
}

- (void)enableResume {
    _playBtn.hidden = NO;
    _pauseBtn.hidden = YES;
}

- (void)disablePauseResume {
    _playBtn.hidden = YES;
    _pauseBtn.hidden = YES;
}

- (BOOL)isPlayEnabled{
    return !_playBtn.hidden;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
