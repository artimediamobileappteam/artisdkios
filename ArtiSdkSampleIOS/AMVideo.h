//
//  AMVideo.h
//  ArtiSdkSampleIOS
//
//  Created by Michael Avoyan on 07/01/2019.
//  Copyright © 2019 Michael Avoyan. All rights reserved.
//

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <ArtiSDK/AMSDK.h>

#define TITLE_START                 @"START"
#define TITLE_START_LIVE_AD_BREAK   @"Start Ad Break Live"
#define TITLE_STOP_LIVE_AD_BREAK    @"Stop Ad Break Live"
#define TITLE_LIVE                  @"Sop Live"
#define TITLE_NOT_LIVE              @"Start Live"

#define LIVE_BREAK_DEFAULT_TIME     @"60"//sec

#define LIVE_BREAK_Y                20//px
#define LIVE_BUTTONS_W              90
#define LIVE_AD_BREAK_BUTTONS_W     170
#define LIVE_BUTTONS_H              35
#define LIVE_TEXT_FIELD_W           45

#define VIDEO_H_PERC                50//%

#define PROGRESSBAR_W_PERC          60//%
#define PROGRESSBAR_H               10//px

#define START_SIZE                  60
#define PLAYER_CONTROLLERS_SIZE     43
#define CONTROLLERS_MARGIN_TOP      10
#define CONTROLLERS_MARGIN_SIDE     5

#define MOVIES_BUTTONS_W            180
#define MOVIES_BUTTONS_H            43

#define URL_NEW_PLAYER_MOVIE_1      @"http://akamai.advsnx.net/CDN/demo.test/media/bbb.mp4"
#define URL_NEW_PLAYER_MOVIE_2      @"https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8"
#define URL_SAME_PLAYER_MOVIE_1     URL_NEW_PLAYER_MOVIE_1
#define URL_SAME_PLAYER_MOVIE_2     URL_NEW_PLAYER_MOVIE_2

#define UPDATE_CONTENT_TIME_DELAY   1.0

#define PLAYER_RATE_PLAYS           1.0

@interface AMVideo  :  UIViewController

@property (nonatomic, strong) UIScrollView* bgSV;
@property (nonatomic, strong) AVPlayerViewController *playerViewController;
@property (nonatomic, copy) NSString* currentContentURL;
@property (nonatomic, strong) UIButton* btnIsLive;
@property (nonatomic, strong) UITextField* tfLiveAdBreakTime;
@property (nonatomic, strong) UIButton* btnAdBreakLive;
@property (nonatomic) BOOL isLive;
@property (nonatomic, strong) UIButton *btnNewPlayerMovie1;
@property (nonatomic, strong) UIButton *btnNewPlayerMovie2;
@property (nonatomic, strong) UIButton *btnSamePlayerMovie1;
@property (nonatomic, strong) UIButton *btnSamePlayerMovie2;
@property (nonatomic, strong) UIButton* playBtn;
@property (nonatomic, strong) UIButton* pauseBtn;
@property (nonatomic, strong) UIProgressView* progressBar;
@property (nonatomic, strong) UIButton* startBtn;
@property (nonatomic, strong) UIView* contentVideoView;
@property (nonatomic, strong) NSMutableDictionary* contentPositionMap;

- (void)onPause;
- (void)onResume;
- (void)allocUI;
- (void)initUiFrames;
- (void)START;
- (void)enablePause;
- (void)enableResume;
- (void)resumeContent;
- (void)pauseContent;
- (void)onPausePressed;
- (void)onResumePressed;
- (void)startMediaPlayerContent;
- (void)seekToCurrentUrlPosition;
- (float)getCurrentContentPosition;
- (float)getCurrentContentDuration;
- (void)updateUI;
- (void)onNewMovie:(NSString*)url;
- (void)playerFailedToPlay:(NSNotification*)notification;
- (void)playerPlaybackDidFinish:(NSNotification*)notification;
- (void)updateCurrentPositionMap:(float)position;
- (void)updateSeekBar:(float)position deuration:(float)duration;
- (void)setContentUrl:(NSString*)url;
- (void)setUrlToPlayer:(NSString*)url updateDelay:(float)updateDelay;
- (void)onIsLive;
- (void)enableLive;
- (void)disableLive;
- (void)updateLiveButtons;
- (void)updateLinearAdBreakTitle:(BOOL)isOn;
- (float)getLiveBreakDuration;

@end
