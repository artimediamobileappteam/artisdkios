//
//  AppDelegate.m
//  ArtiMediaAMSdkIOS
//
//  Created by Michael Avoyan on 24/08/2016.
//  Copyright © 2016 Michael Avoyan. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    [AMAppDelegate application:application performFetchWithCompletionHandler:completionHandler];
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{ 
    // Override point for customization after application launch.
    
//=================================== START iOS 10 ===================================
//#ifdef __IPHONE_10_0
//    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
//    {
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
//        {
//            if( !error )
//            {
//                [[UIApplication sharedApplication] registerForRemoteNotifications];
//            }
//        }];
//    }
//#endif
//=================================== END iOS 10 ===================================
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//=================================== START iOS 10 ===================================
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
//    
//    //Called when a notification is delivered to a foreground app.
//    
//    NSLog(@"Userinfo %@",notification.request.content.userInfo);
//    
//    completionHandler(UNNotificationPresentationOptionAlert);
//}
//
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
//    
//    //Called to let your app know which action was selected by the user for a given notification.
//    
//    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
//    
//}
//=================================== END iOS 10 ===================================

@end
